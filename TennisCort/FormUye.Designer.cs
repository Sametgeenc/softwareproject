﻿namespace TennisCort
{
    partial class FormUye
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormUye));
            this.label1 = new System.Windows.Forms.Label();
            this.btnKortKirala = new System.Windows.Forms.Button();
            this.btnBilgiler = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(113, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(102, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Hoşgeldiniz";
            // 
            // btnKortKirala
            // 
            this.btnKortKirala.Location = new System.Drawing.Point(49, 200);
            this.btnKortKirala.Name = "btnKortKirala";
            this.btnKortKirala.Size = new System.Drawing.Size(75, 39);
            this.btnKortKirala.TabIndex = 1;
            this.btnKortKirala.Text = "Kort Kirala";
            this.btnKortKirala.UseVisualStyleBackColor = true;
            this.btnKortKirala.Click += new System.EventHandler(this.btnKortKirala_Click);
            // 
            // btnBilgiler
            // 
            this.btnBilgiler.Location = new System.Drawing.Point(200, 200);
            this.btnBilgiler.Name = "btnBilgiler";
            this.btnBilgiler.Size = new System.Drawing.Size(75, 39);
            this.btnBilgiler.TabIndex = 2;
            this.btnBilgiler.Text = "Bilgilerim";
            this.btnBilgiler.UseVisualStyleBackColor = true;
            this.btnBilgiler.Click += new System.EventHandler(this.btnBilgiler_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(143, 89);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "label2";
            // 
            // FormUye
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(326, 303);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnBilgiler);
            this.Controls.Add(this.btnKortKirala);
            this.Controls.Add(this.label1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormUye";
            this.Text = "SBF KORT";
            this.Load += new System.EventHandler(this.FormUye_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnKortKirala;
        private System.Windows.Forms.Button btnBilgiler;
        private System.Windows.Forms.Label label2;
    }
}