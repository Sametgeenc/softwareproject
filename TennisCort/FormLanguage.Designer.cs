﻿namespace TennisCort
{
    partial class FormLanguage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormLanguage));
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.picBoxEnglish = new System.Windows.Forms.PictureBox();
            this.picBoxTurkish = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxEnglish)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxTurkish)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(79, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(137, 29);
            this.label1.TabIndex = 2;
            this.label1.Text = "Dil Seçiniz";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.Location = new System.Drawing.Point(12, 198);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(271, 29);
            this.label2.TabIndex = 3;
            this.label2.Text = "Select Your Language";
            // 
            // picBoxEnglish
            // 
            this.picBoxEnglish.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBoxEnglish.Location = new System.Drawing.Point(171, 103);
            this.picBoxEnglish.Name = "picBoxEnglish";
            this.picBoxEnglish.Size = new System.Drawing.Size(69, 50);
            this.picBoxEnglish.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxEnglish.TabIndex = 1;
            this.picBoxEnglish.TabStop = false;
            this.picBoxEnglish.Click += new System.EventHandler(this.picBoxEnglish_Click);
            this.picBoxEnglish.MouseLeave += new System.EventHandler(this.picBoxEnglish_MouseLeave);
            this.picBoxEnglish.MouseHover += new System.EventHandler(this.picBoxEnglish_MouseHover);
            // 
            // picBoxTurkish
            // 
            this.picBoxTurkish.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picBoxTurkish.ErrorImage = null;
            this.picBoxTurkish.InitialImage = global::TennisCort.Properties.Resources.turkish;
            this.picBoxTurkish.Location = new System.Drawing.Point(36, 103);
            this.picBoxTurkish.Name = "picBoxTurkish";
            this.picBoxTurkish.Size = new System.Drawing.Size(69, 50);
            this.picBoxTurkish.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picBoxTurkish.TabIndex = 0;
            this.picBoxTurkish.TabStop = false;
            this.picBoxTurkish.Click += new System.EventHandler(this.picBoxTurkish_Click);
            this.picBoxTurkish.MouseLeave += new System.EventHandler(this.picBoxTurkish_MouseLeave);
            this.picBoxTurkish.MouseHover += new System.EventHandler(this.picBoxTurkish_MouseHover);
            // 
            // FormLanguage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(295, 261);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.picBoxEnglish);
            this.Controls.Add(this.picBoxTurkish);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FormLanguage";
            this.Text = "FormLanguage";
            this.Load += new System.EventHandler(this.FormLanguage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxEnglish)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxTurkish)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox picBoxTurkish;
        private System.Windows.Forms.PictureBox picBoxEnglish;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}