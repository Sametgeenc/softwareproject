﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TennisCort
{
    public partial class FormUye : Form
    {

        public FormUye()
        {
            InitializeComponent();
        }

        private void FormUye_Load(object sender, EventArgs e)
        {

        }

        private void btnKortKirala_Click(object sender, EventArgs e)
        {
            FormKortlar frm = new FormKortlar();
            this.Hide();
            frm.Show();
        }

        private void btnBilgiler_Click(object sender, EventArgs e)
        {
            FormBilgilerim frm = new FormBilgilerim();
            this.Hide();
            frm.Show();
        }
    }
}
