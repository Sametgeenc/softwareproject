﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TennisCort
{
    public partial class FormLogin : Form
    {
        public FormLogin()
        {
            InitializeComponent();
        }

        private void FormLogin_Load(object sender, EventArgs e)
        {

        }

        private void bynKayit_Click(object sender, EventArgs e)
        {
            CreateUser frm = new CreateUser();
            this.Hide();
            frm.Show();
        }

        private void btnGiris_Click(object sender, EventArgs e)
        {
            FormUser frm = new FormUser();
            this.Hide();
            frm.Show();
        }
    }
}
