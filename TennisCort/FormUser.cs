﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TennisCort
{
    public partial class FormUser : Form
    {
        public FormUser()
        {
            InitializeComponent();
        }

        private void btnKortKirala_Click(object sender, EventArgs e)
        {
            FormKorts frm = new FormKorts();
            this.Hide();
            frm.Show();
        }

        private void btnBilgiler_Click(object sender, EventArgs e)
        {
            FormMyInfo frm = new FormMyInfo();
            this.Hide();
            frm.Show();
        }
    }
}
