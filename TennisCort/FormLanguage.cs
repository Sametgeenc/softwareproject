﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TennisCort.Properties;

namespace TennisCort
{
    public partial class FormLanguage : Form
    {
        Image Turkish = Resources.turkish;
        Image English = Resources.english;
        public FormLanguage()
        {
            InitializeComponent();
            picBoxTurkish.Image = Turkish;
            picBoxEnglish.Image = English;
        }

        private void FormLanguage_Load(object sender, EventArgs e)
        {
            Stream str = Properties.Resources.TennisSound;
            SoundPlayer snd = new SoundPlayer(str);
            snd.Play();
        }

        private void picBoxTurkish_Click(object sender, EventArgs e)
        {
            FormGiris frm = new FormGiris();
            this.Hide();
            frm.Show();
        }

        private void picBoxEnglish_Click(object sender, EventArgs e)
        {
            FormLogin frm = new FormLogin();
            this.Hide();
            frm.Show();
        }

        private void picBoxTurkish_MouseHover(object sender, EventArgs e)
        {
            int Turkish_Width = Turkish.Width + ((Turkish.Width + 200) / 1);
            int Turkish_Height = Turkish.Height + ((Turkish.Height + 200) / 1);

            Bitmap Turkish_1 = new Bitmap(Turkish_Width, Turkish_Height);
            Graphics g = Graphics.FromImage(Turkish_1);
            g.DrawImage(Turkish, new Rectangle(Point.Empty, Turkish_1.Size));
            picBoxTurkish.Image = Turkish_1;
        }

        private void picBoxEnglish_MouseHover(object sender, EventArgs e)
        {
            int English_Width = English.Width + ((English.Width + 2000) / 1);
            int English_Height = English.Height + ((English.Height + 2000) / 1);

            Bitmap English_1 = new Bitmap(English_Width, English_Height);
            Graphics g = Graphics.FromImage(English_1);
            g.DrawImage(English, new Rectangle(Point.Empty, English_1.Size));
            picBoxEnglish.Image = English_1;
        }

        private void picBoxTurkish_MouseLeave(object sender, EventArgs e)
        {
            picBoxTurkish.Image = Turkish;
        }

        private void picBoxEnglish_MouseLeave(object sender, EventArgs e)
        {
            picBoxEnglish.Image = English;
        }
    }
}
